\contentsline {section}{\numberline {1}Podstawowe informacje o projekcie}{2}{section.1}
\contentsline {section}{\numberline {2}Podzia\IeC {\l } prac w ramach zespo\IeC {\l }u}{2}{section.2}
\contentsline {section}{\numberline {3}Za\IeC {\l }o\IeC {\.z}enia projektu}{2}{section.3}
\contentsline {subsection}{\numberline {3.1}Elementy logiczne systemu}{2}{subsection.3.1}
\contentsline {subsection}{\numberline {3.2}Elementy fizyczne systemu}{3}{subsection.3.2}
\contentsline {section}{\numberline {4}Przebieg prac nad projektem}{4}{section.4}
\contentsline {section}{\numberline {5}Wykorzystane technologie}{5}{section.5}
\contentsline {subsection}{\numberline {5.1}Rozpoznawanie obrazu}{5}{subsection.5.1}
\contentsline {subsection}{\numberline {5.2}Przechwytywanie obrazu z kamery}{6}{subsection.5.2}
\contentsline {subsection}{\numberline {5.3}Przesy\IeC {\l }anie danych i obrazu na serwer}{7}{subsection.5.3}
\contentsline {subsection}{\numberline {5.4}Przesy\IeC {\l }anie stanu gry do przegl\IeC {\k a}darki}{7}{subsection.5.4}
\contentsline {subsection}{\numberline {5.5}Wizualizacja stanu gry}{8}{subsection.5.5}
\contentsline {section}{\numberline {6}Om\IeC {\'o}wienie implementacji}{8}{section.6}
\contentsline {subsection}{\numberline {6.1}Akwizycja obrazu}{8}{subsection.6.1}
\contentsline {subsection}{\numberline {6.2}Rozpoznawanie obrazu}{9}{subsection.6.2}
\contentsline {subsection}{\numberline {6.3}Przechwytywanie obrazu z kamery}{10}{subsection.6.3}
\contentsline {subsection}{\numberline {6.4}Klient TCP}{12}{subsection.6.4}
\contentsline {subsection}{\numberline {6.5}W\IeC {\k e}ze\IeC {\l } centralny}{13}{subsection.6.5}
\contentsline {subsection}{\numberline {6.6}Sprawdzanie poprawno\IeC {\'s}ci ruchu}{14}{subsection.6.6}
\contentsline {subsubsection}{\numberline {6.6.1}Struktura modu\IeC {\l }u}{15}{subsubsection.6.6.1}
\contentsline {subsubsection}{\numberline {6.6.2}G\IeC {\l }\IeC {\'o}wne struktury danych}{16}{subsubsection.6.6.2}
\contentsline {subsubsection}{\numberline {6.6.3}Klasa \texttt {CheckMove}}{17}{subsubsection.6.6.3}
\contentsline {subsubsection}{\numberline {6.6.4}Klasa \texttt {TreeGenerator} w module \texttt {attack}}{20}{subsubsection.6.6.4}
\contentsline {subsubsection}{\numberline {6.6.5}Funkcje globalne modu\IeC {\l }u \texttt {attack}}{20}{subsubsection.6.6.5}
\contentsline {subsubsection}{\numberline {6.6.6}Powi\IeC {\k a}zania mi\IeC {\k e}dzy funkcjami}{21}{subsubsection.6.6.6}
\contentsline {subsection}{\numberline {6.7}Klient wizualizuj\IeC {\k a}cy stan gry}{21}{subsection.6.7}
\contentsline {subsubsection}{\numberline {6.7.1}Struktura projektu}{21}{subsubsection.6.7.1}
\contentsline {subsubsection}{\numberline {6.7.2}G\IeC {\l }\IeC {\'o}wne struktury danych}{22}{subsubsection.6.7.2}
\contentsline {subsubsection}{\numberline {6.7.3}Najwa\IeC {\.z}niejsze metody analizy stanu planszy}{23}{subsubsection.6.7.3}
\contentsline {subsubsection}{\numberline {6.7.4}Najwa\IeC {\.z}niejsze metody modyfikacji obiektu Canvas}{24}{subsubsection.6.7.4}
\contentsline {subsubsection}{\numberline {6.7.5}Algorytm przetwarzania stanu gry}{25}{subsubsection.6.7.5}
\contentsline {section}{\numberline {7}Napotkane problemy}{26}{section.7}
\contentsline {subsection}{\numberline {7.1}Przechwytywanie obraz\IeC {\'o}w}{26}{subsection.7.1}
\contentsline {subsection}{\numberline {7.2}Analiza obraz\IeC {\'o}w}{26}{subsection.7.2}
\contentsline {subsection}{\numberline {7.3}Klient wizualizuj\IeC {\k a}cy stan gry}{27}{subsection.7.3}
\contentsline {section}{\numberline {8}Instrukcja instalacji}{27}{section.8}
\contentsline {subsection}{\numberline {8.1}Klient wizualizuj\IeC {\k a}cy stan gry}{27}{subsection.8.1}
\contentsline {section}{\numberline {9}Wykorzystane materia\IeC {\l }y}{27}{section.9}
\contentsline {subsection}{\numberline {9.1}Klient wizualizuj\IeC {\k a}cy stan gry}{27}{subsection.9.1}
\contentsline {subsubsection}{\numberline {9.1.1}Grafika}{27}{subsubsection.9.1.1}
\contentsline {subsubsection}{\numberline {9.1.2}\IeC {\'Z}r\IeC {\'o}d\IeC {\l }a}{28}{subsubsection.9.1.2}
