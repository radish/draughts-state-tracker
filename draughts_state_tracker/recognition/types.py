from enum import IntEnum
from typing import NamedTuple

import numpy as np

HSHistograms = NamedTuple('HSHistograms', (('wh_man_r', np.array),
                                           ('wh_man_g', np.array),
                                           ('wh_man_b', np.array),
                                           ('wh_king_r', np.array),
                                           ('wh_king_g', np.array),
                                           ('wh_king_b', np.array),
                                           ('bl_man_r', np.array),
                                           ('bl_man_g', np.array),
                                           ('bl_man_b', np.array),
                                           ('bl_king_r', np.array),
                                           ('bl_king_g', np.array),
                                           ('bl_king_b', np.array),
                                           )
                          )


class Colors(IntEnum):
    white = 0
    black = 1


class States(IntEnum):
    empty = 0
    white_man = 1
    black_man = 2
    white_king = 3
    black_king = 4
