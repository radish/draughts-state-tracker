import numpy as np
from skimage.transform import ProjectiveTransform

from draughts_state_tracker.recognition.types import Colors


def get_coordinates_of_fields(board_box):
    width = board_box[2] - board_box[0]
    height = board_box[3] - board_box[1]

    field_width = width // 10
    field_height = height // 10

    fields = []

    current_y = board_box[1]
    for _ in range(10):
        current_x = board_box[0]

        row = []
        for __ in range(10):
            row.append((current_x + 3, current_y + 3,
                        current_x + field_width - 3,
                        current_y + field_height - 3))
            current_x += field_width

        fields.append(row)
        current_y += field_height

    return fields


def get_transformation(points):
    src = np.array((
        (0, 0),
        (0, 300),
        (300, 300),
        (300, 0)
    ))

    transform = ProjectiveTransform()
    transform.estimate(src, np.array(points))

    return transform


def get_rotation(fields_coords, grayscale_frame):
    corners_coords = (
        (0, 0),
        (9, 0),
        (0, 9),
        (9, 9)
    )

    corners_colors = []
    for corner_coords in corners_coords:
        x, y = corner_coords
        coords = fields_coords[x][y]
        field = grayscale_frame[
                    coords[0]:coords[2], coords[1]:coords[3]
                ]

        (black, white), _ = np.histogram(field, 2)

        if black > white:
            corners_colors.append(Colors.black)
        else:
            corners_colors.append(Colors.white)

    if corners_colors[0] == Colors.black:
        return -90
    if corners_colors[2] == Colors.black:
        return 180
    if corners_colors[3] == Colors.black:
        return 90

    return 0
