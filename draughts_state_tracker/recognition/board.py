import asyncio
from typing import List, Tuple

import matplotlib
import numpy as np
from scipy.spatial.distance import euclidean, cityblock
from skimage.color import rgb2hsv, rgb2gray

from draughts_state_tracker.recognition.exceptions import HandPresent
from draughts_state_tracker.recognition.types import Colors, HSHistograms, \
    States
from draughts_state_tracker.recognition.utils import get_transformation, \
    get_coordinates_of_fields, get_rotation

from skimage.filters.rank import median

matplotlib.use('Qt5Agg')

from skimage.transform import warp, rotate

from matplotlib import pyplot as plt
from skimage import measure, morphology


class Board:
    def __init__(self, event_loop: asyncio.BaseEventLoop):
        self._loop = event_loop

        self._frame_mask = None
        self._background = None
        self._transformation = None
        self._grayscale_frame = None
        self._rgb_frame = None
        self._fields_boxes = None
        self._rotation = None
        self._grayscale_frame = None
        self._rgb_frame = None
        self._fields_map = None
        self._rgb_hists = None

    def _get_fields_map(self) -> List[Colors]:
        colors = []

        for i, row in enumerate(self._fields_boxes):
            colors.append([])
            for j, field in enumerate(row):
                if i % 2 == 0 and j % 2 == 0 or i % 2 != 0 and j % 2 != 0:
                    colors[i].append(Colors.white)
                else:
                    colors[i].append(Colors.black)

        return colors

    def _get_rgb_histogram_for_field(self, rows: Tuple[int], frame, msk):
        r = np.zeros(256, np.int64)
        g = np.zeros(256, np.int64)
        b = np.zeros(256, np.int64)

        for row_number in rows:
            for field_number in range(9):
                if self._fields_map[row_number][field_number] == Colors.black:
                    pawn = self._fields_boxes[row_number][field_number]
                    field = frame[pawn[0]:pawn[2], pawn[1]:pawn[3]]
                    mask = msk[pawn[0]:pawn[2], pawn[1]:pawn[3]]

                    r += np.histogram(
                        np.ma.masked_array(field[:, :, 0], mask), 256
                    )[0]
                    g += np.histogram(
                        np.ma.masked_array(field[:, :, 1], mask), 256
                    )[0]
                    b += np.histogram(
                        np.ma.masked_array(field[:, :, 2], mask), 256
                    )[0]
        wh_man_div = 5 * len(rows)
        r //= wh_man_div
        g //= wh_man_div
        b //= wh_man_div

        return r, g, b

    def _get_rgb_histograms(self) -> HSHistograms:
        difference = np.fabs(np.subtract(self._frame_mask, self._background))
        mask = median(difference < 0.07, morphology.disk(1))

        frame = self._rgb_frame

        wh_man_rows = (0, 1, 2)
        wh_man_r, wh_man_g, wh_man_b = self._get_rgb_histogram_for_field(
            wh_man_rows, frame, mask
        )

        wh_king_rows = (3,)
        wh_king_r, wh_king_g, wh_king_b = self._get_rgb_histogram_for_field(
            wh_king_rows, frame, mask
        )

        bl_man_rows = (7, 8, 9)
        bl_man_r, bl_man_g, bl_man_b = self._get_rgb_histogram_for_field(
            bl_man_rows, frame, mask
        )

        bl_king_rows = (6,)
        bl_king_r, bl_king_g, bl_king_b = self._get_rgb_histogram_for_field(
            bl_king_rows, frame, mask
        )

        return HSHistograms(wh_man_r, wh_man_g, wh_man_b,
                            wh_king_r, wh_king_g, wh_king_b,
                            bl_man_r, bl_man_g, bl_man_b,
                            bl_king_r, bl_king_g, bl_king_b)

    def _get_fields_states(self) -> List[List[int]]:
        states = []
        frame_mask = rotate(self._frame_mask, self._rotation)

        for i, row in enumerate(self._fields_boxes):
            states.append([])
            for j, field_box in enumerate(row):
                field = self._rgb_frame[
                            field_box[0]:field_box[2], field_box[1]:field_box[3]
                        ]
                field_mask = frame_mask[
                                 field_box[0]:field_box[2],
                                 field_box[1]:field_box[3]
                             ]
                masked_r = np.ma.masked_array(field[:, :, 0], field_mask)
                masked_g = np.ma.masked_array(field[:, :, 1], field_mask)
                masked_b = np.ma.masked_array(field[:, :, 2], field_mask)

                r_hist, _ = np.histogram(masked_r, 256)
                g_hist, _ = np.histogram(masked_g, 256)
                b_hist, _ = np.histogram(masked_b, 256)

                if self._fields_map[i][j] == Colors.white:
                    if np.count_nonzero(field_mask) < field.size // 6:
                        states[i].append(int(States.black_man))
                    else:
                        states[i].append(int(States.empty))
                else:
                    if np.count_nonzero(field_mask) < field.size // 6:
                        white_man = (
                            euclidean(r_hist, self._rgb_hists.wh_man_r) *
                            euclidean(g_hist, self._rgb_hists.wh_man_g) *
                            euclidean(b_hist, self._rgb_hists.wh_man_b) +
                            cityblock(r_hist, self._rgb_hists.wh_man_r) *
                            cityblock(g_hist, self._rgb_hists.wh_man_g) *
                            cityblock(b_hist, self._rgb_hists.wh_man_b)
                        )
                        white_king = (
                            euclidean(r_hist, self._rgb_hists.wh_king_r) *
                            euclidean(g_hist, self._rgb_hists.wh_king_g) *
                            euclidean(b_hist, self._rgb_hists.wh_king_b) +
                            cityblock(r_hist, self._rgb_hists.wh_king_r) *
                            cityblock(g_hist, self._rgb_hists.wh_king_g) *
                            cityblock(b_hist, self._rgb_hists.wh_king_b)
                        )

                        black_man = (
                            euclidean(r_hist, self._rgb_hists.bl_man_r) *
                            euclidean(g_hist, self._rgb_hists.bl_man_g) *
                            euclidean(b_hist, self._rgb_hists.bl_man_b) +
                            cityblock(r_hist, self._rgb_hists.bl_man_r) *
                            cityblock(g_hist, self._rgb_hists.bl_man_g) *
                            cityblock(b_hist, self._rgb_hists.bl_man_b)
                        )
                        black_king = (
                            euclidean(r_hist, self._rgb_hists.bl_king_r) *
                            euclidean(g_hist, self._rgb_hists.bl_king_g) *
                            euclidean(b_hist, self._rgb_hists.bl_king_b) +
                            cityblock(r_hist, self._rgb_hists.bl_king_r) *
                            cityblock(g_hist, self._rgb_hists.bl_king_g) *
                            cityblock(b_hist, self._rgb_hists.bl_king_b)
                        )

                        index = np.argmin((white_man, white_king,
                                           black_man, black_king))
                        if index == 0:
                            states[i].append(int(States.white_man))
                        elif index == 1:
                            states[i].append(int(States.white_king))
                        elif index == 2:
                            states[i].append(int(States.black_man))
                        else:
                            states[i].append(int(States.black_king))
                    else:
                        states[i].append(int(States.empty))

        return states

    def _check_if_hand_present(self, grayscale_frame: np.ndarray) -> bool:
        diff = np.fabs(np.subtract(grayscale_frame, self._grayscale_frame))
        filtered = median(diff < 0.07, morphology.disk(1))
        inactive = np.count_nonzero(filtered) / grayscale_frame.size

        return inactive < 0.97

    def recognize_empty_board(self, initial_frame_rgb: np.ndarray):
        initial_frame_grayscale = rgb2gray(initial_frame_rgb)

        points = get_board_properties(initial_frame_rgb)
        self._transformation = get_transformation(points)
        self._grayscale_frame = warp(initial_frame_grayscale,
                                     self._transformation,
                                     output_shape=(300, 300))
        self._rgb_frame = warp(initial_frame_rgb, self._transformation,
                               output_shape=(300, 300))

        self._frame_mask = self._grayscale_frame
        self._background = self._grayscale_frame

        self._fields_boxes = get_coordinates_of_fields((0, 0, 300, 300))
        self._fields_map = self._get_fields_map()

        plt.imshow(self._rgb_frame)
        plt.show()

    def recognize_draughts(self, frame):
        self._grayscale_frame = warp(rgb2gray(frame), self._transformation,
                                     output_shape=(300, 300))
        self._frame_mask = self._grayscale_frame

        self._rotation = get_rotation(self._fields_boxes, self._grayscale_frame)
        self._rgb_frame = rotate(
            warp(frame, self._transformation, output_shape=(300, 300)),
            self._rotation
        )

        plt.imshow(self._rgb_frame)
        plt.show()

        self._rgb_hists = self._get_rgb_histograms()

    def process_frame(self, frame: np.ndarray) -> List[List[int]]:
        grayscale_frame = warp(rgb2gray(frame), self._transformation,
                               output_shape=(300, 300))

        if self._check_if_hand_present(grayscale_frame) is True:
            raise HandPresent

        difference = np.fabs(np.subtract(grayscale_frame, self._background))
        mask = median(difference < 0.07, morphology.disk(1))

        rotated_board = rotate(
            warp(frame, self._transformation, output_shape=(300, 300)),
            self._rotation
        )

        self._grayscale_frame = grayscale_frame
        self._rgb_frame = rotated_board
        self._frame_mask = mask

        return self._get_fields_states()


def get_board_properties(frame):
    hsv_frame = rgb2hsv(frame)

    mask = ((0.11, 0.7, 0.7) < hsv_frame) & (hsv_frame <= (0.17, 1, 1))
    thresholded = np.zeros(mask.shape[:2], dtype=np.bool)

    # TODO: there has to be a better way to do it (this is very slow)
    for i, row in enumerate(mask):
        for j, pixel in enumerate(row):
            if pixel.all():
                thresholded[i][j] = True

    labeled_regions = measure.label(thresholded, background=False)
    regions_props = measure.regionprops(labeled_regions)
    regions_props.sort(key=lambda p: p.filled_area, reverse=True)
    regions_props = regions_props[:4]

    top_down = sorted(regions_props, key=lambda p: p.bbox[0])

    ordered_props = sorted(top_down[:2], key=lambda p: p.bbox[1])
    ordered_props.extend(
        sorted(top_down[2:], key=lambda p: p.bbox[1], reverse=True)
    )

    return [
        (ordered_props[0].bbox[3], ordered_props[0].bbox[2]),
        (ordered_props[1].bbox[1], ordered_props[1].bbox[2]),
        (ordered_props[2].bbox[1], ordered_props[2].bbox[0]),
        (ordered_props[3].bbox[3], ordered_props[3].bbox[0]),
    ]
