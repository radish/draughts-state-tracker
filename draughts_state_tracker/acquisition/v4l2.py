import asyncio

import numpy as np
from PIL import Image
from PyV4L2Camera.camera import Camera as V4L2Camera, CameraError
from PyV4L2Camera.controls import ControlIDs

from draughts_state_tracker.acquisition.camera import AbstractCamera


class Camera(AbstractCamera):
    def __init__(self, event_loop: asyncio.BaseEventLoop,
                 camera_number: int = 0):
        self._loop = event_loop
        self._camera = V4L2Camera('/dev/video{}'.format(camera_number),
                                  1920, 1080)
        self._camera.set_control_value(ControlIDs.SATURATION, 255)
        self._camera.set_control_value(ControlIDs.SHARPNESS, 255)
        self._camera.set_control_value(ControlIDs.AUTO_WHITE_BALANCE, 1)

        try:
            for _ in range(120):
                self.grab_image()
        except CameraError:
            pass

        self._camera.set_control_value(ControlIDs.AUTO_WHITE_BALANCE, 0)

    def grab_image(self) -> np.ndarray:
        return np.asarray(
            Image.frombytes('RGB', (self._camera.width, self._camera.height),
                            self._camera.get_frame(), 'raw', 'RGB')
        )

    def close(self):
        self._camera.close()
