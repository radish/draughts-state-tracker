import asyncio

import cv2
import numpy as np

from draughts_state_tracker.acquisition.camera import AbstractCamera


class Camera(AbstractCamera):
    def __init__(self, event_loop: asyncio.BaseEventLoop,
                 camera_number: int = 0):
        self._loop = event_loop
        self._camera = cv2.VideoCapture()

        status = self._camera.open(camera_number)
        if not status:
            raise RuntimeError('Cannot open camera')

        self._camera.set(cv2.CAP_PROP_FRAME_WIDTH, 1920)
        self._camera.set(cv2.CAP_PROP_FRAME_HEIGHT, 1080)

    def grab_image(self) -> np.ndarray:
        # TODO: test if running in executor gives any overall performance boost
        status, image = self._camera.read()

        if not status:
            raise RuntimeError('Camera returned status: {0}'.format(status))

        return np.roll(image, 1, axis=-1)

    def close(self):
        self._camera.release()
