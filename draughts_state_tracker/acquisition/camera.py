from abc import ABCMeta, abstractmethod


class AbstractCamera(metaclass=ABCMeta):
    @abstractmethod
    def grab_image(self):
        ...

    @abstractmethod
    def close(self):
        ...
