import asyncio
from io import BytesIO
from typing import List

import msgpack
import numpy as np
from PIL import Image


class TCPClient:
    def __init__(self, event_loop: asyncio.BaseEventLoop, host: str,
                 port: int):
        self._loop = event_loop
        self._host = host
        self._port = port
        self._buffer = BytesIO()
        self._reader = None  # type: asyncio.StreamReader
        self._writer = None  # type: asyncio.StreamWriter

    async def open_connection(self):
        self._reader, self._writer = await asyncio.streams.open_connection(
            self._host, self._port, loop=self._loop
        )

    async def send_data(self, data: List[List[int]]):
        message = msgpack.packb({
            '_type': 0,
            'payload': data
        }, use_bin_type=True)
        message_length = len(message).to_bytes(4, byteorder='big')

        self._writer.write(message_length + message)
        await self._writer.drain()

    def send_frame(self, frame: np.ndarray):
        image = Image.fromarray(frame)
        image.save(self._buffer, 'JPEG')
        payload = self._buffer.getvalue()

        message = msgpack.packb({
            '_type': 1,
            'payload': payload
        }, use_bin_type=True)
        message_length = len(message).to_bytes(4, byteorder='big')

        self._writer.write(message_length + message)

    async def request_empty_board(self):
        message = msgpack.packb({
            '_type': 3,
            'payload': None
        }, use_bin_type=True)
        message_length = len(message).to_bytes(4, byteorder='big')

        self._writer.write(message_length + message)
        await self._writer.drain()

    async def request_initial_setting(self):
        message = msgpack.packb({
            '_type': 4,
            'payload': None
        }, use_bin_type=True)
        message_length = len(message).to_bytes(4, byteorder='big')

        self._writer.write(message_length + message)
        await self._writer.drain()

    async def request_game_start(self):
        message = msgpack.packb({
            '_type': 5,
            'payload': None
        }, use_bin_type=True)
        message_length = len(message).to_bytes(4, byteorder='big')

        self._writer.write(message_length + message)
        await self._writer.drain()

    async def close_connection(self):
        await self._writer.drain()
        self._writer.close()
        await asyncio.sleep(0.5)
