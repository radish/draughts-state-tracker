from draughts_state_tracker.moves_validation.field import FieldState
from draughts_state_tracker.moves_validation.point import Point, Point2PointKill
from copy import deepcopy


def find_killing_path(tree_root, from_point, to_point, kills):
    return _find_killing_path(tree_root, from_point, to_point, kills, [])


def _find_killing_path(tree_root, from_point, to_point, kills, path):
    # if leaf is found, stop recursion
    if not tree_root.children:
        # found destination, return move path
        if not kills:
            return path
        else:
            return None

    for node in tree_root.children:
        if (node.value.fr == from_point and
           kills and
           node.value.kills in kills):
            path.append(from_point)
            kills.remove(node.value.kills)
            ret_path = _find_killing_path(node, node.value.to, to_point, kills, path)
            # correct kill path is found, end searching of killing path
            if ret_path:
                return ret_path
            path.pop()

    # we didn't find any matching path
    return None


def maximum_captures(tree_root):
    return _max_capt(tree_root, 0)


def _max_capt(tree_root, captured):
    if not tree_root.children:
        return captured

    max_capt = 0
    for node in tree_root.children:
        capt = _max_capt(node, captured + 1)
        if capt > max_capt:
            max_capt = capt

    return max_capt


def filter_captures_lt(tree_root, value):
    _filter_captures_lt(tree_root, value, 0)


def _filter_captures_lt(tree_root, value, captured):
    if not tree_root.children:
        if captured < value:
            # mark to delete
            tree_root.value = None
        return

    to_delete = []
    for node in tree_root.children:
        _filter_captures_lt(node, value, captured + 1)
        if node.is_empty():
            to_delete.append(node)

    for node in to_delete:
        tree_root.remove_child(node)

    # this branch doesn't meet "less than" criterion, mark to remove
    if not tree_root.children:
        tree_root.value = None


class TreeGenerator:
    def __init__(self, man_pos, enemy, current):
        self.tree_root = Node()
        self.enemy = enemy
        if enemy == FieldState.BLACK_PLAYER:
            self.enemy_king = FieldState.BLACK_PLAYER_KING
        else:
            self.enemy_king = FieldState.WHITE_PLAYER_KING

        # make copy of original data, because we don't want to edit original one
        self.current = deepcopy(current)
        self.start_man_pos = man_pos

    def man_possible_attacks(self):
        # temporary remove tested man from board to consider its position
        temp = self.current[self.start_man_pos.x][self.start_man_pos.y]
        self.current[self.start_man_pos.x][self.start_man_pos.y] = FieldState.EMPTY

        self.__man_pos_atks(self.start_man_pos, self.tree_root)

        # restore tested man
        self.current[self.start_man_pos.x][self.start_man_pos.y] = temp

        return self.tree_root

    def __man_pos_atks(self, man_pos, tree_root):
        # white left forward attack move, black right backward attack move
        try:
            x = man_pos.x - 1
            # get around python slicing behaviour, we need bound checking here
            if x < 0:
                raise IndexError()
            y = man_pos.y + 1
            if (self.current[x][y] == self.enemy or
               self.current[x][y] == self.enemy_king):
                enemy_pos = Point(x, y)
                x = man_pos.x - 2
                if x < 0:
                    raise IndexError()
                y = man_pos.y + 2
                if self.current[x][y] == FieldState.EMPTY:
                    new_man_pos = Point(x, y)

                    temp = self.current[enemy_pos.x][enemy_pos.y]

                    # hypothetic kill
                    self.current[enemy_pos.x][enemy_pos.y] = FieldState.EMPTY

                    node = Node(Point2PointKill(man_pos, new_man_pos, enemy_pos))
                    tree_root.add_child(node)

                    # check recursively possibility of attack from new place
                    self.__man_pos_atks(new_man_pos, node)

                    # restore state before capture
                    self.current[enemy_pos.x][enemy_pos.y] = temp
        except IndexError:
            pass

        # white right forward attack move, black left backward attack move
        try:
            x = man_pos.x + 1
            # get around python slicing behaviour, we need bound checking here
            if x < 0:
                raise IndexError()
            y = man_pos.y + 1
            if (self.current[x][y] == self.enemy or
               self.current[x][y] == self.enemy_king):
                enemy_pos = Point(x, y)
                x = man_pos.x + 2
                if x < 0:
                    raise IndexError()
                y = man_pos.y + 2
                if self.current[x][y] == FieldState.EMPTY:
                    new_man_pos = Point(x, y)

                    temp = self.current[enemy_pos.x][enemy_pos.y]

                    # hypothetic kill
                    self.current[enemy_pos.x][enemy_pos.y] = FieldState.EMPTY

                    node = Node(Point2PointKill(man_pos, new_man_pos, enemy_pos))
                    tree_root.add_child(node)

                    # check recursively possibility of attack from new place
                    self.__man_pos_atks(new_man_pos, node)

                    # restore state before killing
                    self.current[enemy_pos.x][enemy_pos.y] = temp
        except IndexError:
            pass

        # white left backward attack move, black right forward attack move
        try:
            x = man_pos.x - 1
            # get around python slicing behaviour, we need bound checking here
            if x < 0:
                raise IndexError()
            y = man_pos.y - 1
            if (self.current[x][y] == self.enemy or
               self.current[x][y] == self.enemy_king):
                enemy_pos = Point(x, y)
                x = man_pos.x - 2
                if x < 0:
                    raise IndexError()
                y = man_pos.y - 2
                if self.current[x][y] == FieldState.EMPTY:
                    new_man_pos = Point(x, y)

                    temp = self.current[enemy_pos.x][enemy_pos.y]

                    # hypothetic kill
                    self.current[enemy_pos.x][enemy_pos.y] = FieldState.EMPTY

                    node = Node(Point2PointKill(man_pos, new_man_pos, enemy_pos))
                    tree_root.add_child(node)

                    # check recursively possibility of attack from new place
                    self.__man_pos_atks(new_man_pos, node)

                    # restore state before killing
                    self.current[enemy_pos.x][enemy_pos.y] = temp
        except IndexError:
            pass

        # white right backward attack move, black left forward attack move
        try:
            x = man_pos.x + 1
            # get around python slicing behaviour, we need bound checking here
            if x < 0:
                raise IndexError()
            y = man_pos.y - 1
            if (self.current[x][y] == self.enemy or
               self.current[x][y] == self.enemy_king):
                enemy_pos = Point(x, y)
                x = man_pos.x + 2
                if x < 0:
                    raise IndexError()
                y = man_pos.y - 2
                if self.current[x][y] == FieldState.EMPTY:
                    new_man_pos = Point(x, y)

                    temp = self.current[enemy_pos.x][enemy_pos.y]

                    # hypothetic kill
                    self.current[enemy_pos.x][enemy_pos.y] = FieldState.EMPTY

                    node = Node(Point2PointKill(man_pos, new_man_pos, enemy_pos))
                    tree_root.add_child(node)

                    # check recursively possibility of attack from new place
                    self.__man_pos_atks(new_man_pos, node)

                    # restore state before killing
                    self.current[enemy_pos.x][enemy_pos.y] = temp
        except IndexError:
            pass

    def king_possible_attacks(self):
        # temporary remove tested king from board to consider its position
        temp = self.current[self.start_man_pos.x][self.start_man_pos.y]
        self.current[self.start_man_pos.x][self.start_man_pos.y] = FieldState.EMPTY

        self.__king_possible_attacks(self.start_man_pos, self.tree_root)

        # restore tested man
        self.current[self.start_man_pos.x][self.start_man_pos.y] = temp

        return self.tree_root

    def __king_possible_attacks(self, king_pos, tree_root):

        # left forward move
        try:
            # if king captured piece, he is able to continue move in the other
            # direction on condition that he can capture next piece

            # scan until enemy is found
            x = king_pos.x
            y = king_pos.y
            while(True):
                x -= 1
                if x < 0:
                    raise IndexError()
                y += 1
                if (self.current[x][y] == self.enemy or
                   self.current[x][y] == self.enemy_king):
                    enemy_pos = Point(x, y)
                    x -= 1
                    if x < 0:
                        raise IndexError()
                    y += 1
                    if self.current[x][y] == FieldState.EMPTY:
                        new_king_pos = Point(x, y)

                        temp = self.current[enemy_pos.x][enemy_pos.y]
                        # kings can't go through killed enemy, so we need mark
                        # enemys as killed before their removal from board
                        self.current[enemy_pos.x][enemy_pos.y] = FieldState.KILLED

                        # king can stop killing at any empty field
                        for xx, yy in zip(range(new_king_pos.x, -1, -1), range(new_king_pos.y, 10)):
                            if self.current[xx][yy] != FieldState.EMPTY:
                                break
                            else:
                                new_king_pos = Point(xx, yy)
                                node = Node(Point2PointKill(king_pos, new_king_pos, enemy_pos))
                                tree_root.add_child(node)

                                # check recursively possibility of attack from new place
                                self.__king_possible_attacks(new_king_pos, node)

                        # restore state before killing
                        self.current[enemy_pos.x][enemy_pos.y] = temp

                    # enemy was found or unjumpable path was encountered,
                    # so we don't need to search further
                    break
                elif self.current[x][y] == FieldState.EMPTY:
                    continue
                else:
                    # killed or own piece was found, so we can't continue move
                    break;
        except IndexError:
            pass

        # right forward move
        try:
            # if king captured piece, he is able to continue move in the other
            # direction on condition that he can capture next piece

            # scan until enemy is found
            x = king_pos.x
            y = king_pos.y
            while(True):
                x += 1
                if x < 0:
                    raise IndexError()
                y += 1
                if (self.current[x][y] == self.enemy or
                   self.current[x][y] == self.enemy_king):
                    enemy_pos = Point(x, y)
                    x += 1
                    if x < 0:
                        raise IndexError()
                    y += 1
                    if self.current[x][y] == FieldState.EMPTY:
                        new_king_pos = Point(x, y)

                        temp = self.current[enemy_pos.x][enemy_pos.y]
                        # kings can't go through killed enemy, so we need mark
                        # enemys as killed before their removal from board
                        self.current[enemy_pos.x][enemy_pos.y] = FieldState.KILLED

                        # king can stop killing at any empty field
                        for xx, yy in zip(range(new_king_pos.x, 10), range(new_king_pos.y, 10)):
                            if self.current[xx][yy] != FieldState.EMPTY:
                                break
                            else:
                                new_king_pos = Point(xx, yy)
                                node = Node(Point2PointKill(king_pos, new_king_pos, enemy_pos))
                                tree_root.add_child(node)

                                # check recursively possibility of attack from new place
                                self.__king_possible_attacks(new_king_pos, node)

                        # restore state before killing
                        self.current[enemy_pos.x][enemy_pos.y] = temp

                    # enemy was found or unjumpable path was encountered,
                    # so we don't need to search further
                    break
                elif self.current[x][y] == FieldState.EMPTY:
                    continue
                else:
                    # killed or own piece was found, so we can't continue move
                    break;
        except IndexError:
            pass

        # white left backward move
        try:
            # if king captured piece, he is able to continue move in the other
            # direction on condition that he can capture next piece

            # scan until enemy is found
            x = king_pos.x
            y = king_pos.y
            while(True):
                x -= 1
                if x < 0:
                    raise IndexError()
                y -= 1
                if (self.current[x][y] == self.enemy or
                   self.current[x][y] == self.enemy_king):
                    enemy_pos = Point(x, y)
                    x -= 1
                    if x < 0:
                        raise IndexError()
                    y -= 1
                    if self.current[x][y] == FieldState.EMPTY:
                        new_king_pos = Point(x, y)

                        temp = self.current[enemy_pos.x][enemy_pos.y]
                        # kings can't go through killed enemy, so we need mark
                        # enemys as killed before their removal from board
                        self.current[enemy_pos.x][enemy_pos.y] = FieldState.KILLED

                        # king can stop killing at any empty field
                        for xx, yy in zip(range(new_king_pos.x, -1, -1), range(new_king_pos.y, -1, -1)):
                            if self.current[xx][yy] != FieldState.EMPTY:
                                break
                            else:
                                new_king_pos = Point(xx, yy)
                                node = Node(Point2PointKill(king_pos, new_king_pos, enemy_pos))
                                tree_root.add_child(node)

                                # check recursively possibility of attack from new place
                                self.__king_possible_attacks(new_king_pos, node)

                        # restore state before killing
                        self.current[enemy_pos.x][enemy_pos.y] = temp

                    # enemy was found or unjumpable path was encountered,
                    # so we don't need to search further
                    break
                elif self.current[x][y] == FieldState.EMPTY:
                    continue
                else:
                    # killed or own piece was found, so we can't continue move
                    break;
        except IndexError:
            pass

        # white right backward move
        try:
            # if king captured piece, he is able to continue move in the other
            # direction on condition that he can capture next piece

            # scan until enemy is found
            x = king_pos.x
            y = king_pos.y
            while(True):
                x += 1
                if x < 0:
                    raise IndexError()
                y -= 1
                if (self.current[x][y] == self.enemy or
                   self.current[x][y] == self.enemy_king):
                    enemy_pos = Point(x, y)
                    x += 1
                    if x < 0:
                        raise IndexError()
                    y -= 1
                    if self.current[x][y] == FieldState.EMPTY:
                        new_king_pos = Point(x, y)

                        temp = self.current[enemy_pos.x][enemy_pos.y]
                        # kings can't go through killed enemy, so we need mark
                        # enemys as killed before their removal from board
                        self.current[enemy_pos.x][enemy_pos.y] = FieldState.KILLED

                        # king can stop killing at any empty field
                        for xx, yy in zip(range(new_king_pos.x, 10), range(new_king_pos.y, -1, -1)):
                            if self.current[xx][yy] != FieldState.EMPTY:
                                break
                            else:
                                new_king_pos = Point(xx, yy)
                                node = Node(Point2PointKill(king_pos, new_king_pos, enemy_pos))
                                tree_root.add_child(node)

                                # check recursively possibility of attack from new place
                                self.__king_possible_attacks(new_king_pos, node)

                        # restore state before killing
                        self.current[enemy_pos.x][enemy_pos.y] = temp

                    # enemy was found or unjumpable path was encountered,
                    # so we don't need to search further
                    break
                elif self.current[x][y] == FieldState.EMPTY:
                    continue
                else:
                    # killed or own piece was found, so we can't continue move
                    break;
        except IndexError:
            pass


class Node:
    def __init__(self, value=None):
        self.value = value
        self.children = []

    def add_child(self, child):
        self.children.append(child)

    def remove_child(self, child):
        self.children.remove(child)

    def __repr__(self, level=0):
        treestr = "    " * level + repr(self.value) + "\n"
        for node in self.children:
            treestr += node.__repr__(level + 1)
        return treestr

    def is_empty(self):
        return (not self.value and not self.children)
