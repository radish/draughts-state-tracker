from collections import namedtuple

Point = namedtuple('Point', ['x', 'y'])
Point2Point = namedtuple('P2P', ['fr', 'to'])
Point2PointKill = namedtuple('P2PKill', ['fr', 'to', 'kills'])
