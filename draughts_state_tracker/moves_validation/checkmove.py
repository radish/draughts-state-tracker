from draughts_state_tracker.moves_validation import attack
from draughts_state_tracker.moves_validation.field import FieldState
from draughts_state_tracker.moves_validation.point import (Point,
                                                           Point2Point)


"""
Loaded data[x][y] layout

        0,2,0,2,0,2,0,2,0,2 <--- data[9][9]
        2,0,2,0,2,0,2,0,2,0
        0,2,0,2,0,2,0,2,0,2
        2,0,2,0,2,0,2,0,2,0
        0,0,0,0,0,0,0,0,0,0
        0,0,0,0,0,0,0,0,0,0
        0,1,0,1,0,1,0,1,0,1
y       1,0,1,0,1,0,1,0,1,0
        0,1,0,1,0,1,0,1,0,1
^       1,0,1,0,1,0,1,0,1,0
|
|     data[0][0]...data[9][0]
-----> x
1 - white player
2 - black player

"""


def draughts_layout_print(data):
    for y in range(9, -1, -1):
        print(str(y) + '. ', end='')
        for x in range(0, 10):

            print(str(data[x][y]) + ',', end='')

        print()
    print()


class CheckMove:
    DIRECTION_UP = 1
    DIRECTION_DOWN = -1

    def __init__(self, path=None):
        self.current = None
        self.data = []
        self.path = path
        self.turn = None
        if path:
            self.__load_data(path)

    def __load_data(self, path):
        with open(path, 'r') as f:
            # board_state = [[0] * 10] * 10 is true evil,
            # see http://stackoverflow.com/a/6667529

            board_state = [[None for x in range(10)] for y in range(10)]
            y = 9
            for line in f:
                if (y == -1):
                    self.data.append(board_state)
                    board_state = [[None for x in range(10)] for y in range(10)]
                    y = 9
                    continue

                row = line.strip().split(',')
                for x in range(0, 10):
                    board_state[x][y] = int(row[x])

                y -= 1

    @staticmethod
    def convert_board_state(board_state):
        '''Converts to internal representation of board_state'
        '''
        # rotate to right about 90 degree
        converted_board_state = [[None for x in range(10)] for y in range(10)]
        board_x = 0
        board_y = 9

        conv_board_x = 0
        conv_board_y = 0
        while conv_board_y < 10:
            board_y = 9
            conv_board_x = 0
            while board_y >= 0:
                converted_board_state[conv_board_x][conv_board_y] = board_state[board_x][board_y]
                board_y -= 1
                conv_board_x += 1

            board_x += 1
            conv_board_y += 1

        return converted_board_state

    # 0 - nothing change
    # 1 - wygrana pierwszego
    # 2 - drugiego
    # 3 - blad pierwszego
    # 4 - blad drugiego
    # 5 - pierwszy musi bic maksymalnie
    # 6 - drugi wymuszony atak
    def is_correct(self, board_state):
        # board_state = CheckMove.convert_board_state(board_state)
        # it is initial state, so we only check if draughts are on white field
        if not self.current:
            self.current = board_state
            # set turn to BLACK_PLAYER so game will be started by WHITE_PLAYER
            self.turn = FieldState.BLACK_PLAYER
            return self.is_all_on_black_field()
        else:
            # next player turn
            if self.turn == FieldState.WHITE_PLAYER:
                self.turn = FieldState.BLACK_PLAYER
            else:
                self.turn = FieldState.WHITE_PLAYER

            if self.is_all_on_black_field():
                print(self.turn, "moves")

                # TODO: handle P2P(fr=None, to=None, kills=a lot) case
                # TODO: add invocation of methods for checking kings moves
                move, kills = self.extract_move_from_board_state(board_state, self.turn)
                print(move, kills)
                pos_atks = self.possible_attacks(self.turn)
                if not pos_atks:
                    pos_diag_moves = self.possible_diagonals(self.turn)
                    if not kills and move in pos_diag_moves:
                        self.current = board_state
                        return None
                    print("Bad move. You can't move diagonally in that way.")
                else:
                    for tree in pos_atks:
                        #print("Attack tree")
                        #print(tree)
                        path = attack.find_killing_path(tree, move.fr, move.to, kills)
                        if path:
                            print(path)
                            self.current = board_state
                            return None
                    print("Bad move. There is forced attack on board.")

        if self.turn == FieldState.WHITE_PLAYER:
            # white player have to try again, prepare turn
            self.turn = FieldState.BLACK_PLAYER
            return 3
        else:
            # white player have to try again, prepare turn
            self.turn = FieldState.WHITE_PLAYER
            return 4

    def is_all_on_black_field(self):
        # check if any of draugthsmen is on white field, if no assume,
        # that all of them are on black fields
        for i in range(0, 10):
            white_field_offset = (i + 1) % 2
            for j in range(0, 10, 2):
                if self.current[j + white_field_offset][i] != FieldState.EMPTY:
                    return False
        return True

    def possible_diagonals(self, player):
        # TODO: possible diagonals should include kings move
        possible_moves = []

        player_king = None
        if player == FieldState.WHITE_PLAYER:
            player_king = FieldState.WHITE_PLAYER_KING
        else:
            player_king = FieldState.BLACK_PLAYER_KING

        for i in range(0, 10):
            black_field_offset = i % 2
            for j in range(0, 10, 2):
                if self.current[j + black_field_offset][i] == player:
                    man = Point(j + black_field_offset, i)
                    possible_moves.extend(self.man_possible_diagonals(man, player))
                elif self.current[j + black_field_offset][i] == player_king:
                    king = Point(j + black_field_offset, i)
                    possible_moves.extend(self.man_possible_diagonals(king, player))

        return possible_moves

    def man_possible_diagonals(self, man_pos, player):
        possible_moves = []
        direction = None
        if player == FieldState.WHITE_PLAYER:
            direction = CheckMove.DIRECTION_UP
        elif player == FieldState.BLACK_PLAYER:
            direction = CheckMove.DIRECTION_DOWN

        # white - left diagonal move, black - right diagonal move
        try:
            x = man_pos.x - 1
            # get around python slicing behaviour, we need bound checking here
            if x < 0:
                raise IndexError()
            y = man_pos.y + direction
            if self.current[x][y] == FieldState.EMPTY:
                possible_moves.append(Point2Point(man_pos, Point(x, y)))
        except IndexError:
            pass

        # white - right diagonal move, black - left diagonal move
        try:
            x = man_pos.x + 1
            y = man_pos.y + direction
            if self.current[x][y] == FieldState.EMPTY:
                possible_moves.append(Point2Point(man_pos, Point(x, y)))
        except IndexError:
            pass

        return possible_moves

    def king_possible_diagonals(self, king_pos, player):
        possible_moves = []
        # left forward move
        for x, y in zip(range(king_pos.x - 1, -1, -1), range(king_pos.y + 1, 10)):
            if self.current[x][y] != FieldState.EMPTY:
                break
            else:
                possible_moves.append(Point2Point(king_pos, Point(x, y)))

        # right forward move
        for x, y in zip(range(king_pos.x + 1, 10), range(king_pos.y + 1, 10)):
            if self.current[x][y] != FieldState.EMPTY:
                break
            else:
                possible_moves.append(Point2Point(king_pos, Point(x, y)))

        # white left backward move
        for x, y in zip(range(king_pos.x - 1, -1, -1), range(king_pos.y - 1, -1, -1)):
            if self.current[x][y] != FieldState.EMPTY:
                break
            else:
                possible_moves.append(Point2Point(king_pos, Point(x, y)))

        # white right backward move
        for x, y in zip(range(king_pos.x + 1, 10), range(king_pos.y - 1, -1, -1)):
            if self.current[x][y] != FieldState.EMPTY:
                break
            else:
                possible_moves.append(Point2Point(king_pos, Point(x, y)))

        print(possible_moves)
        return possible_moves

    def possible_attacks(self, player):
        possible_moves = []
        player_king = None
        if player == FieldState.WHITE_PLAYER:
            player_king = FieldState.WHITE_PLAYER_KING
        else:
            player_king = FieldState.BLACK_PLAYER_KING

        for i in range(0, 10):
            black_field_offset = i % 2
            for j in range(0, 10, 2):
                if self.current[j + black_field_offset][i] == player:
                    man = Point(j + black_field_offset, i)

                    atk_tree = self.man_possible_attacks(man, player)

                    if not atk_tree.is_empty():
                        possible_moves.append(atk_tree)
                elif self.current[j + black_field_offset][i] == player_king:
                    king = Point(j + black_field_offset, i)
                    atk_tree = self.king_possible_attacks(king, player)

                    if not atk_tree.is_empty():
                        possible_moves.append(atk_tree)

        max_captures = 0
        for item in possible_moves:
            max_captures = max(max_captures, attack.maximum_captures(item))

        for tree in possible_moves:
            attack.filter_captures_lt(tree, max_captures)

        # filter_captures_lt left empty tree roots, so we need remove it
        # from list
        possible_moves = [tree for tree in possible_moves if not tree.is_empty()]

        return possible_moves

    def man_possible_attacks(self, man_pos, player):
        enemy = None
        if player == FieldState.WHITE_PLAYER:
            enemy = FieldState.BLACK_PLAYER
        elif player == FieldState.BLACK_PLAYER:
            enemy = FieldState.WHITE_PLAYER

        attack_tree = attack.TreeGenerator(man_pos, enemy, self.current)
        attack_tree_possible_moves = attack_tree.man_possible_attacks()

        return attack_tree_possible_moves

    def king_possible_attacks(self, king_pos, player):
        enemy = None
        if player == FieldState.WHITE_PLAYER:
            enemy = FieldState.BLACK_PLAYER
        elif player == FieldState.BLACK_PLAYER:
            enemy = FieldState.WHITE_PLAYER

        attack_tree = attack.TreeGenerator(king_pos, enemy, self.current)
        attack_tree_possible_moves = attack_tree.king_possible_attacks()

        return attack_tree_possible_moves

    def extract_move_from_board_state(self, intern_board_state, player):
        """
        Point2Point(fr=None, to=None) with non-empty list of kills means
        that piece returned to same position after multiple kills, but we
        aren't able to determine what piece was moved.
        """

        enemy = None
        if player == FieldState.WHITE_PLAYER:
            player_king = FieldState.WHITE_PLAYER_KING
            enemy = FieldState.BLACK_PLAYER
            enemy_king = FieldState.BLACK_PLAYER_KING
        elif player == FieldState.BLACK_PLAYER:
            player_king = FieldState.BLACK_PLAYER_KING
            enemy = FieldState.WHITE_PLAYER
            enemy_king = FieldState.WHITE_PLAYER_KING

        from_point = None
        to_point = None

        kills = []

        for i in range(0, 10):
            black_field_offset = i % 2
            for j in range(0, 10, 2):
                x = j + black_field_offset
                y = i

                # look for changes between current and new board state
                if self.current[x][y] != intern_board_state[x][y]:

                    # starting point of player move
                    if ((self.current[x][y] == player or
                       self.current[x][y] == player_king) and
                       intern_board_state[x][y] == FieldState.EMPTY):

                        from_point = Point(x, y)

                    # ending point of player move
                    if (self.current[x][y] == FieldState.EMPTY and
                       (intern_board_state[x][y] == player or
                           intern_board_state[x][y] == player_king)):

                        to_point = Point(x, y)

                    # captures a.k.a. kills
                    if ((self.current[x][y] == enemy or
                       self.current[x][y] == enemy_king) and
                       intern_board_state[x][y] == FieldState.EMPTY):

                        kills.append(Point(x, y))

        return Point2Point(from_point, to_point), kills

if __name__ == '__main__':
    checker = CheckMove('moves_data/game/game_04.txt')
    checker.turn = FieldState.WHITE_PLAYER
    i = 0
    for datum in checker.data:
        # if checker.current is None:
        #     checker.current = datum

        #if checker.turn == FieldState.WHITE_PLAYER:
        #    checker.turn = FieldState.BLACK_PLAYER
        #else:
        #    checker.turn = FieldState.WHITE_PLAYER

        #print(checker.possible_attacks(FieldState.WHITE_PLAYER))
        print(print(checker.is_correct(datum)))
        move, kills = checker.extract_move_from_board_state(datum, checker.turn)

        draughts_layout_print(datum)
        # checker.current = datum
        print(move, kills)
        print()
        print()



