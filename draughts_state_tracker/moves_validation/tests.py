import unittest
from checkmove import CheckMove
from field import FieldState
from point import Point


class ValidationOfMoveTestCase(unittest.TestCase):

    def test_all_on_black(self):
        valid_test_files = [
            'moves_data/diagonals/white_diagonal.txt',
            'moves_data/black_fields/black_fields_valid.txt'
        ]
        invalid_test_files = [
            'moves_data/black_fields/black_fields_invalid.txt'
        ]

        for path in valid_test_files:
            test_no = 1
            checker = CheckMove(path)
            for datum in checker.data:
                checker.current = datum
                with self.subTest(path=path, test_no=test_no):
                    self.assertTrue(checker.is_all_on_black_field())
                test_no += 1

        for path in invalid_test_files:
            test_no = 1
            checker = CheckMove(path)
            for datum in checker.data:
                checker.current = datum
                with self.subTest(path=path, test_no=test_no):
                    self.assertFalse(checker.is_all_on_black_field())
                test_no += 1

    def test_valid_white_diagonal_moves(self):
        test_files = [
            'moves_data/diagonals/white_diagonal.txt'
        ]

        golden_files = [
            'moves_data/diagonals/white_diagonal_expected.txt'
        ]

        self.maxDiff = None

        golden_strings = []
        for path in golden_files:
            input_text = ""
            with open(path, 'r') as f:
                for line in f:
                    input_text += line

            golden_strings.append(input_text)

        for path in test_files:
            checker = CheckMove(path)
            test_string = ""
            for datum in checker.data:
                checker.current = datum

                moves = checker.possible_diagonals(FieldState.WHITE_PLAYER)
                for move in moves:
                    test_string += str(move) + "\n"
                test_string += "\n"

            self.assertMultiLineEqual(test_string, golden_strings[0])

    def test_diagonal_one_move_allowed(self):
        test_files = [
                'moves_data/diagonals/diagonal_one_or_two_move_allowed.txt'
                ]

        golden_files = [
                'moves_data/diagonals/diagonal_one_or_two_move_allowed_expected.txt'
                ]

        self.maxDiff = None

        golden_strings = []
        for path in golden_files:
            input_text = ""
            with open(path, 'r') as f:
                for line in f:
                    input_text += line

            golden_strings.append(input_text)

        for path in test_files:
            checker = CheckMove(path)
            test_string = ""
            for datum in checker.data:
                checker.current = datum

                moves = checker.possible_diagonals(FieldState.WHITE_PLAYER)
                for move in moves:
                    test_string += str(move) + "\n"
                test_string += "\n"

            for datum in checker.data:
                checker.current = datum

                moves = checker.possible_diagonals(FieldState.BLACK_PLAYER)
                for move in moves:
                    test_string += str(move) + "\n"
                test_string += "\n"

            self.assertMultiLineEqual(test_string, golden_strings[0])

    def test_man_attack(self):
        test_files = [
                'moves_data/attacks/white_3_men_4_attacks.txt',
                'moves_data/attacks/white_3_men_1_attack_possible.txt'
                ]

        golden_files = [
                'moves_data/attacks/white_3_men_4_attacks_expected.txt',
                'moves_data/attacks/white_3_men_1_attack_possible_expected.txt'
                ]

        self.maxDiff = None

        golden_strings = []
        for path in golden_files:
            input_text = ""
            with open(path, 'r') as f:
                for line in f:
                    input_text += line

            golden_strings.append(input_text)

        test_no = 0
        for path in test_files:
            with self.subTest(path=path, test_no=test_no):
                checker = CheckMove(path)
                test_string = ""
                for datum in checker.data:
                    checker.current = datum

                    moves = checker.possible_attacks(FieldState.WHITE_PLAYER)
                    for move in moves:
                        test_string += str(move) + "\n"
                    test_string += "\n"
                self.assertMultiLineEqual(test_string, golden_strings[test_no])
            test_no += 1

    def test_king_possible_attacks(self):
        test_files = [
                'moves_data/attacks/white_king_complicated_attack.txt',
                'moves_data/attacks/white_king_same_line.txt'
                ]

        golden_files = [
                'moves_data/attacks/white_king_complicated_attack_expected.txt',
                'moves_data/attacks/white_king_same_line_expected.txt'
                ]

        self.maxDiff = None

        golden_strings = []
        for path in golden_files:
            input_text = ""
            with open(path, 'r') as f:
                for line in f:
                    input_text += line

            golden_strings.append(input_text)

        test_no = 0
        for path in test_files:
            with self.subTest(path=path, test_no=test_no):
                checker = CheckMove(path)
                test_string = ""
                for datum in checker.data:
                    checker.current = datum

                    moves = checker.possible_attacks(FieldState.WHITE_PLAYER)
                    for move in moves:
                        test_string += str(move) + "\n"
                    test_string += "\n"
                self.assertMultiLineEqual(test_string, golden_strings[test_no])
            test_no += 1

    def test_king_all_possible_attacks(self):
        test_files = [
                'moves_data/attacks/black_king_complicated_attack.txt',
                'moves_data/attacks/black_king_same_line.txt'
                ]

        golden_files = [
                'moves_data/attacks/black_king_complicated_attack_expected.txt',
                'moves_data/attacks/black_king_same_line_expected.txt'
                ]

        self.maxDiff = None

        golden_strings = []
        for path in golden_files:
            input_text = ""
            with open(path, 'r') as f:
                for line in f:
                    input_text += line

            golden_strings.append(input_text)

        test_no = 0
        for path in test_files:
            with self.subTest(path=path, test_no=test_no):
                checker = CheckMove(path)
                test_string = ""
                for datum in checker.data:
                    checker.current = datum
                    test_string = str(checker.king_possible_attacks(Point(8, 0), FieldState.BLACK_PLAYER))
                    
                self.assertMultiLineEqual(test_string, golden_strings[test_no])
            test_no += 1

    def test_king_19_attacks(self):
        test_files = [
                'moves_data/attacks/white_19_attacks.txt'
                ]

        golden_files = [
                'moves_data/attacks/white_19_attacks_expected.txt'
                ]

        self.maxDiff = None

        golden_strings = []
        for path in golden_files:
            input_text = ""
            with open(path, 'r') as f:
                for line in f:
                    input_text += line

            golden_strings.append(input_text)

        test_no = 0
        for path in test_files:
            with self.subTest(path=path, test_no=test_no):
                checker = CheckMove(path)
                test_string = ""
                for datum in checker.data:
                    checker.current = datum
                    test_string = str(checker.possible_attacks(FieldState.WHITE_PLAYER))
                    print(test_string)
                    
                self.assertMultiLineEqual(test_string, golden_strings[test_no])
            test_no += 1


if __name__ == '__main__':
    unittest.main()
