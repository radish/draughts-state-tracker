#!/usr/bin/env python3

from setuptools import setup, find_packages

from draughts_state_tracker import __version__

setup(
    name='draughts-state-viewer',
    version=__version__,
    url='https://gitlab.com/radish/draughts-state-tracker',
    install_requires=[
        'uvloop',
        'numpy',
        'scikit-image',
        'scipy',
        'matplotlib',
        'msgpack-python',
        'PyV4L2Camera',
    ],
    extras_require={
      'Picamera based capture': ['picamera'],
    },
    packages=find_packages(),
    scripts=['bin/dst', ]
)
